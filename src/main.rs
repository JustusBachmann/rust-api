use std::{hash::Hash, collections::HashMap};
use actix_web::{get, web::{self, Json}, App, HttpServer, Responder, HttpResponse, error, post, put};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct Interface {
    interface_id: u32,
}

#[derive(Serialize, Deserialize)]
struct Network {
    network_id: u8,
    chip_id: u32,
}

#[get("/")]
async fn index() -> impl Responder {
    "API Endpoint for Distributed Embedded Systems!"
}

#[get("/status")]
async fn status() -> impl Responder {
    let mut map = HashMap::new();

    map.insert("status", "ok");
    map.insert("version", "0.1.0");
    map.insert("433MHz", "ok");
    map.insert("868MHz", "ok");
    
    HttpResponse::Ok().json(map)
}

#[get("/interfaces")]
async fn interfaces() -> impl Responder {
    let mut map = HashMap::new();
    map.insert("433MHz", "ok");
    map.insert("868MHz", "ok");
    
    HttpResponse::Ok().json(map)
}


#[put("/client/interface/{interface_id}/connect")]
async fn connect(info: web::Path<Interface>, data: Json<Network>) -> impl Responder {
    // let mut map = HashMap::new();
    println!("interface_id: {}", info.interface_id);
    HttpResponse::Ok().json(data)
}

#[get("/client/interface/{interface_id}/status")]
async fn interface_status(info: web::Path<Interface>) -> impl Responder {
    let mut map = HashMap::new();
    map.insert("message", "connected".to_string());
    
    HttpResponse::Ok().json(map)
}

#[put("/client/interface/{interface_id}/disconnect")]
async fn disconnect(info: web::Path<Interface>) -> impl Responder {
    let mut map = HashMap::new();
    map.insert("interface-id", format!("{}", info.interface_id));
    map.insert("message", "disconnected".to_string());
    
    HttpResponse::Ok().json(map)
}


#[get("/server/interface/{interface_id}/chips")]
async fn chips(info: web::Path<Interface>) -> impl Responder {
    let mut map = HashMap::new();
    map.insert("addr", "[ip_addr, timestamp]");
    
    HttpResponse::Ok().json(map)
}

#[get("/server/interface/{interface_id}/status")]
async fn server_status(info: web::Path<Interface>) -> impl Responder {
    let mut map = HashMap::new();
    map.insert(info.interface_id, "ok");
    
    HttpResponse::Ok().json(map)
}

#[get("/usage_statistics")]
async fn usage_statistics() -> impl Responder {
    let mut map = HashMap::new();
    map.insert("packets", "[433MHz: 0, 868MHz: 0]");
    map.insert("bytes", "[433MHz: 0, 868MHz: 0]");
    
    HttpResponse::Ok().json(map)
}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        let json_config = web::JsonConfig::default()
            .limit(4096)
            .error_handler(|err, _req| {
                // create custom error response
                error::InternalError::from_response(err, HttpResponse::Conflict().finish())
                    .into()
            });
            App::new()
                .app_data(json_config)
                .service(index)
                .service(status)
                .service(interfaces)
                .service(connect)
                .service(interface_status)
                .service(disconnect)
                .service(chips)
                .service(usage_statistics)
                .service(server_status)
        })
        .bind(("127.0.0.1", 8000))?
        .run()
        .await
    
}